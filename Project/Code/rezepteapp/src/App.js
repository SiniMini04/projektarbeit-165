import "./App.css";
import HEADER from "./header.js";
import List from "./list.js";

function App() {
  return (
    <>
      <HEADER></HEADER>
      <div className="body">
        <List></List>
      </div>
    </>
  );
}

export default App;
