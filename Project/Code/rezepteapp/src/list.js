import React from "react";
import { useEffect } from "react";
import axios from "axios";
import "./list.css";

function getRezepte() {
  const url = "http://localhost:3001/api/rezepte/getpreview";
  return axios.get(url).then((res) => res.data);
}

function List() {
  useEffect(() => {
    const list = document.getElementById("List");
    getRezepte().then((data) => {
      for (let i = 0; i < data.length; i++) {
        const div = document.createElement("div");
        const rezeptname = document.createElement("h1");
        const rezeptkategorie = document.createElement("h2");
        const bild = document.createElement("img");
        const anleitung = document.createElement("p");
        const erstelldatum = document.createElement("p");
        const aufwandZeit = document.createElement("p");
        const aufwandSchwierigkeit = document.createElement("p");
        const gesammtbewertung = document.createElement("p");
        const br = document.createElement("br");
        const zutatenText = document.createElement("p");

        rezeptname.textContent = data[i].rezeptname;
        rezeptkategorie.textContent = data[i].rezeptkategorie;
        anleitung.textContent = data[i].anleitung;
        erstelldatum.textContent = data[i].erstelldatum;
        aufwandZeit.textContent = "Zeit: " + data[i].aufwand.dauer + " min.";
        aufwandSchwierigkeit.textContent =
          "Aufwand: " + data[i].aufwand.schwierigkeit;
        gesammtbewertung.textContent = data[i].gesammtbewertung;
        if (data[i].bild.includes("data:image/jpeg;base64,")) {
          bild.src = data[i].bild;
        } else {
          bild.src = "data:image/jpeg;base64," + data[i].bild;
        }
        zutatenText.textContent = "Zutaten:";

        div.appendChild(rezeptname);
        div.appendChild(rezeptkategorie);
        div.appendChild(erstelldatum);
        div.appendChild(gesammtbewertung);
        div.appendChild(aufwandZeit);
        div.appendChild(aufwandSchwierigkeit);
        div.appendChild(bild);
        div.appendChild(zutatenText);

        for (let y = 0; y < data[i].zutaten.length; y++) {
          const zutaten = document.createElement("p");

          let zutatenMenge = data[i].zutaten[y].menge;
          if (data[i].zutaten[y].menge === 0) {
            zutatenMenge = "";
          }
          zutaten.textContent =
            data[i].zutaten[y].produkt +
            " " +
            zutatenMenge +
            " " +
            data[i].zutaten[y].einheit;
          div.appendChild(zutaten);
        }
        div.appendChild(br);
        div.appendChild(anleitung);
        list.appendChild(div);
      }
    });
  }, []);
  return (
    <>
      <div id="List" className="List"></div>
    </>
  );
}

export default List;
