import React from "react";
import "./header.css";
import axios from "axios";

function search() {
  const searchInput = document.getElementById("searchInput");
  const input = searchInput.value;
  console.log(input);
  const url = "http://localhost:3001/api/rezepte/postsearch";
  return axios
    .post(url, { search: document.getElementById("searchInput").value })
    .then((res) => res.data); //TODO something with the data
}

function showzutaten() {
  let list = document.getElementById("List");

  list.innerHTML = "";
  search().then((data) => {
    console.log(data);
    for (let i = 0; i < data.length; i++) {
      const div = document.createElement("div");
      const rezeptname = document.createElement("h1");
      const rezeptkategorie = document.createElement("h2");
      const bild = document.createElement("img");
      const anleitung = document.createElement("p");
      const erstelldatum = document.createElement("p");
      const aufwandZeit = document.createElement("p");
      const aufwandSchwierigkeit = document.createElement("p");
      const gesammtbewertung = document.createElement("p");
      const br = document.createElement("br");
      const zutatenText = document.createElement("p");

      rezeptname.textContent = data[i].rezeptname;
      rezeptkategorie.textContent = data[i].rezeptkategorie;
      anleitung.textContent = data[i].anleitung;
      erstelldatum.textContent = data[i].erstelldatum;
      aufwandZeit.textContent = "Zeit: " + data[i].aufwand.dauer + " min.";
      aufwandSchwierigkeit.textContent =
        "Aufwand: " + data[i].aufwand.schwierigkeit;
      gesammtbewertung.textContent = data[i].gesammtbewertung;
      if (data[i].bild.includes("data:image/jpeg;base64,")) {
        bild.src = data[i].bild;
      } else {
        bild.src = "data:image/jpeg;base64," + data[i].bild;
      }
      zutatenText.textContent = "Zutaten:";

      div.appendChild(rezeptname);
      div.appendChild(rezeptkategorie);
      div.appendChild(erstelldatum);
      div.appendChild(gesammtbewertung);
      div.appendChild(aufwandZeit);
      div.appendChild(aufwandSchwierigkeit);
      div.appendChild(bild);
      div.appendChild(zutatenText);

      for (let y = 0; y < data[i].zutaten.length; y++) {
        const zutaten = document.createElement("p");

        let zutatenMenge = data[i].zutaten[y].menge;
        if (data[i].zutaten[y].menge === 0) {
          zutatenMenge = "";
        }
        zutaten.textContent =
          data[i].zutaten[y].produkt +
          " " +
          zutatenMenge +
          " " +
          data[i].zutaten[y].einheit;
        div.appendChild(zutaten);
      }
      div.appendChild(br);
      div.appendChild(anleitung);
      list.appendChild(div);
    }
  });
}

function HEADER() {
  return (
    <>
      <div class="header">
        <header>
          <h1>Rezepte</h1>
          <div class="search-container">
            <input type="text" id="searchInput" placeholder="Search..." />
            <button type="button" id="searchButton" onClick={showzutaten}>
              Search
            </button>
          </div>
        </header>
      </div>
    </>
  );
}

export default HEADER;
