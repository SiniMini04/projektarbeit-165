import axios from "axios";

const url = "http://127.0.0.1:5984";

async function downloadDatafromDB() {
  await axios
    .post(url + "/_session", { name: "admin", password: "123456789" })
    .then(console.log("Test"));

  let daten = (
    await axios.get(url + "/rezepte-sinan-menolfi/_all_docs?include_docs=true")
  ).data;

  let reinedaten = [];

  for (let i = 0; i < daten.rows.length; i++) {
    if (
      daten.rows[i].id !== "_design/search" &&
      daten.rows[i].id !== "_design/validator"
    ) {
      reinedaten.push(daten.rows[i].doc);
    }
  }

  return reinedaten;
}

const daten = downloadDatafromDB(); //Ist asyncron

async function find(s) {
  await axios
    .post(url + "/_session", { name: "admin", password: "123456789" })
    .then(console.log("Test"));
  console.log(s);
  let daten = (
    await axios.post(url + "/rezepte-sinan-menolfi/_find", {
      selector: {
        zutaten: { $elemMatch: { produkt: { $regex: "(?i)" + s } } },
      },
    })
  ).data;

  let reinedaten = [];
  for (let i = 0; i < daten.docs.length; i++) {
    if (
      daten.docs[i].id !== "_design/search" &&
      daten.docs[i].id !== "_design/validator"
    ) {
      reinedaten.push(daten.docs[i]);
    }
  }

  return reinedaten;
}

function getAll() {
  return Promise.resolve(daten);
}

function getFind(s) {
  return Promise.resolve(find(s));
}

export { getAll, getFind };
