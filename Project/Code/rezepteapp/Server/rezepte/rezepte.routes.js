import { Router } from "express";
import { getpreview, postsearch } from "./rezepte.controller.js";

const r = Router();
r.get("/getpreview", getpreview);
r.post("/postsearch", postsearch);

export { r };
