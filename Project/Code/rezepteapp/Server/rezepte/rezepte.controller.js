import { getAll, getFind } from "./rezepte.model.js";

let globRezepte;

async function getpreview(request, response) {
  if (globRezepte != null) {
    response.json(globRezepte);
  } else {
    const rezepte = await getAll();
    globRezepte = rezepte;
    response.json(rezepte);
  }
}
async function postsearch(request, response) {
  const search = await getFind(request.body.search);
  response.json(search);
}

export { getpreview, postsearch };
