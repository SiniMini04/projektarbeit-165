import express from "express";
import { r as rezepteRouter } from "./rezepte/rezepte.routes.js";
import cors from "cors";

const app = express();

app.use(cors());
app.use(express.json());
//Use Routes

app.use("/api/rezepte", rezepteRouter);

app.listen(3001, () => {
  console.log("Server listens to http://localhost:3001");
});
